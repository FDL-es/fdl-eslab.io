# FDL_es
######  GNU FDL en Español
###### GFDL FDL en Español

Licencia de documentación Libre de GNU


Esta es una traducción no oficial de la Licencia de documentación Libre de GNU al español. No ha sido publicada por la Free Software Foundation y no establece los términos jurídicos de distribución del software publicado bajo la FDL de GNU, solo la FDL de GNU original en inglés lo hace. De todos modos, esperamos que esta traducción ayude a los hispanohablantes a comprender mejor la FDL de GNU. 

[ODT](FDL-es.odt) 

[PDF](FDL-es.pdf)

[TXT](FDL-es.txt)

# HTML

[FDL-es.html](https://fdl-es.gitlab.io/FDL-es.html)

[FDL](https://fdl-es.gitlab.io/index.html)

# Repositorio 


###  [https://fdl-es.gitlab.io/](https://fdl-es.gitlab.io/)


# FDL
###### GNU FDL 
###### GFDL

[https://www.gnu.org/licenses/fdl-1.3.en.html](https://www.gnu.org/licenses/fdl-1.3.en.html)


[https://www.gnu.org/licenses/fdl.html](https://www.gnu.org/licenses/fdl.html)

## Licencia de Documentación Libre de GNU

La Licencia de Documentación Libre de GNU es una forma de copyleft pensada para manuales, libros de texto u otros documentos, para garantizar que todo el mundo tenga la libertad de copiar y redistribuir la obra, con o sin modificaciones, de modo comercial o no comercial. La última es la versión 1.3. 

[https://www.gnu.org/licenses/licenses.html#FDL](https://www.gnu.org/licenses/licenses.html#FDL)

# Licencias GNU

[https://www.gnu.org/licenses/licenses.html](https://www.gnu.org/licenses/licenses.html)

[https://www.gnu.org/licenses/licenses.es.html](https://www.gnu.org/licenses/licenses.es.html)
